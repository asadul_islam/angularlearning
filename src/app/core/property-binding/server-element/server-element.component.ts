import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy,
  ViewChild,
  ElementRef,
  ContentChild
} from "@angular/core";
import { Content } from "@angular/compiler/src/render3/r3_ast";

@Component({
  selector: "app-server-element",
  templateUrl: "./server-element.component.html",
  styleUrls: ["./server-element.component.scss"]
})
export class ServerElementComponent
  implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {
  @Input("srvElement") element: { type: string; name: string; content: string };
  @ViewChild('heading') header: ElementRef; 
  @ContentChild('serverContentPara') para: ElementRef; 


  constructor() {
    console.log("Constructor");
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("onChanges");
    console.log(changes);
  }

  ngOnInit() {
    console.log("ngOnInit");
    console.log("Text Content: " + this.header.nativeElement.textContent);
    console.log("Text Content of Paragraph " + this.para.nativeElement.textContent )
  }

  ngDoCheck(): void {
    console.log("NgDoCheck");
  }

  ngAfterContentInit(): void {
    console.log("AfterContentInit");
    console.log("Text Content of Paragraph " + this.para.nativeElement.textContent );
  }

  ngAfterContentChecked(){
    console.log("AfterContentCheck")
  }

  ngAfterViewInit(){
    console.log("ngAfterViewInit");
    console.log("Text Content: " + this.header.nativeElement.textContent)
  }

  ngAfterViewChecked(){
    console.log("ngAfterViewChecked");
  }

  ngOnDestroy(){
    console.log("OnDestroy");
  }

}
