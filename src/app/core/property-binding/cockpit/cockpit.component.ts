import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.scss']
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  @Output('bpCreated') blueprintCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  //newServerName = '';
  //newServerContent = '';
  @ViewChild('serverContentInput') serverContentInput: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  onAddServer(inputServerName: HTMLInputElement){
    this.serverCreated.emit({
      serverName: inputServerName.value, 
      serverContent: this.serverContentInput.nativeElement.value 
    });
  }

  onAddBlueprint(inputServerName){
    this.blueprintCreated.emit({
      serverName: inputServerName.value, 
      serverContent: this.serverContentInput.nativeElement.value 
    });
  }

}
