import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.scss']
})
export class DirectivesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  isOnline: boolean = false;
  status: string = "offline";
  friends = ["Ashraful", "Ripon", "Shuvo"];
  onClick(){
    this.isOnline = !this.isOnline;
    if(this.isOnline){
      this.status = "Online";
    }
    else{
      this.status = "Offline";
    }
  }

  getColor(){
    return this.isOnline ? 'green' : 'red';
  }
}
